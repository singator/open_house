# README #

The purpose of this repo is a presentation on statistics and NBA.

Data on NBA can be obtained from several sources.

## Files ##

### `src` folder ###
1. `half_court.R` contains code that creates an r spatial object of the NBA 
half-court, with all measurements in feet.
2. `shooting_zones.R` contains code that creates an r spatial object of 
shooting zones.

### `data` folder ###
1. The data folder contains the rds files


## More info ##

For more info, do message me or send me an email at stavg AT nus DOT edu DOT sg.